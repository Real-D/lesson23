<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 16:23
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Page;

class PageController extends Controller
{
    public function actionIndex()
    {
        $pages = Page::find()->all();
        return $this->render('index', ['pages' => $pages]);
    }

    public function actionView()
    {
        $id = \Yii::$app->request->get('id');
        $page = Page::findOne($id);
        return $this->render('view', ['page' => $page]);
    }

    //Создание новой записи
    public function actionCreate()
    {
        if (\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();

            if (empty($data['title']) || empty($data['alias']) || empty($data['intro']) || empty($data['content'])){
                return $this->render('create');
            }

            $page = new Page();
            $page->load(['Page' => $data]);
            $page->save();

            $this->redirect('/page/view?id=' . $page->id);
        }
        return $this->render('create');
    }

    //Изменение записи
    public function actionUpdate()
    {
        if (\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();

            if (empty($data['title']) || empty($data['alias']) || empty($data['intro']) || empty($data['content'])){
                return $this->render('update');
            }
            $page = Page::findOne($data['id']);
            $page->title = $data['title'];
            $page->alias = $data['alias'];
            $page->intro = $data['intro'];
            $page->content = $data['content'];
            $page->update();
            $this->redirect('/page/view?id=' . $page->id);
        }

        $id = \Yii::$app->request->get(id);
        $page = Page::findOne($id);
        return $this->render('update', ['page' => $page]);
    }

    //Удаление записи
    public function actionDelete()
    {
        $id = \Yii::$app->request->get('id');
        $page = Page::findOne($id);
        $page->delete();
        $this->redirect('/page/');
    }
}