<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 20:54
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Order;
use app\models\User;

class OrderController extends Controller
{
    public function actionIndex()
    {
        $orders = Order::find()->all();

        return $this->render('index', ['orders' => $orders]);
    }

    //Создание новой записи

    /**
     * @return string
     */
    public function actionCreate()
    {
        if (\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();

          /* var_dump($data);
            die;*/

            if (empty($data['customer_name']) || empty($data['email']) || empty($data['phone']) || empty($data['feedback'])){
                return $this->render('create');
            }

            $order = User::findOne($data['userId']);
            if (!empty($order->username)) {
                $data['customer_name'] = $order->username;
            }
            $order->addOrder($data['customer_name'], $data['email'], $data['phone'], $data['feedback']);

            $this->redirect('/order/');
        }
        return $this->render('create');
    }
}