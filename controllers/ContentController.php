<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 14:18
 */

namespace app\controllers;

use yii\web\Controller;
use app\models\Product;
class ContentController extends Controller
{
    public function actionIndex()
    {
      $products = Product::find()->All();
        return $this->render('index', ['products' => $products]);
    }


    public function actionView()
    {
       $id = \Yii::$app->request->get('id');
        $product = Product::findOne($id);
        return $this->render('view', ['product' => $product]);
    }

    //Создание новой записи
    public function actionCreate()
    {
        if (\Yii::$app->request->isPost){
                $data = \Yii::$app->request->post();

        if (empty($data['title']) || empty($data['alias']) || empty($data['price']) || empty($data['description'])){
            return $this->render('create');
        }

                $product = new Product();
                $product->load(['Product' => $data]);
                $product->save();

                $this->redirect('/content/view?id=' . $product->id);
        }
        return $this->render('create');
    }

    //Изменение записи
    public function actionUpdate()
    {
        if (\Yii::$app->request->isPost){
            $data = \Yii::$app->request->post();

            if (empty($data['title']) || empty($data['alias']) || empty($data['price']) || empty($data['description'])){
                return $this->render('update');
            }
            $product = Product::findOne($data['id']);
            $product->title = $data['title'];
            $product->alias = $data['alias'];
            $product->price = $data['price'];
            $product->description = $data['description'];
            $product->update();
            $this->redirect('/content/view?id=' . $product->id);
        }

        $id = \Yii::$app->request->get(id);
        $product = Product::findOne($id);
        return $this->render('update', ['product' => $product]);
    }

    //Удаление записи
    public function actionDelete()
    {
        $id = \Yii::$app->request->get('id');
        $product = Product::findOne($id);
        $product->delete();
        $this->redirect('/content/');
    }
}