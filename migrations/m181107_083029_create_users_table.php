<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m181107_083029_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(50),
            'password' => $this->string(200),
            'authKey' => $this->string(50),
            'accessToken' => $this->string(50),
        ]);

        $this->insert('users', [
            'username' => 'admin',
            'password' => \Yii::$app->security->generatePasswordHash('admin'),
            'authKey' => 'test200',
            'accessToken' => '100-token',
        ]);

        $this->insert('users', [
            'username' => 'dima',
            'password' => \Yii::$app->security->generatePasswordHash('dima'),
            'authKey' => 'test300',
            'accessToken' => '200-token',
        ]);

        $this->insert('users', [
            'username' => 'user',
            'password' => \Yii::$app->security->generatePasswordHash('user'),
            'authKey' => 'test300',
            'accessToken' => '300-token',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
