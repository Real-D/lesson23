<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m181017_214315_create_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'customer_name' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'feedback' => $this->text(),
        ]);

        $this->insert('orders', [
            'customer_name' => 'John',
            'email' => 'John@mail.ru',
            'phone' => '(068) 356-25-65',
            'feedback' => 'Пожалуйста, подскажите 8 гб там установлено одной планкой?
Спасибо',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Sem',
            'email' => 'Sem@yandex.ru',
            'phone' => '(066) 344-21-63',
            'feedback' => 'У всех потрескивает или сильно шумит hdd?',
        ]);

        $this->insert('orders', [
            'customer_name' => 'Elis',
            'email' => 'Elis@gmail.com',
            'phone' => '(088) 777-12-21',
            'feedback' => 'Можно ли вместо hdd сразу заказать с ссд? И сколько будет стоить?',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }
}
