<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pages`.
 */
class m181017_214456_create_pages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('pages', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->string(),
            'intro' => $this->text(),
            'content' => $this->text(),
        ]);

        $this->insert('pages', [
            'title' => 'Как обновить windows 7 до windows 10',
            'alias' => 'kak-obnovit-windows-7-do-windows-10',
            'intro' => 'Недавно вышла новая операционная система от Microsoft Windows 10. Поэтому у многих заинтересованных пользователей встал закономерный вопрос: Как обновить Windows 7 до Windows 10? Стоит ли обновляться с семёрки до 10 это уже вопрос другой, а если кот уже решил для себя перейти с windows 7 на windows 10 то в этой статье я расскажу как это сделать . Тем более, что компания Microsoft предоставляет обновление в течении года абсолютно бесплатно и всем желающим.',
            'content' => 'Недавно вышла новая операционная система от Microsoft Windows 10. Поэтому у многих заинтересованных пользователей встал закономерный вопрос: Как обновить Windows 7 до Windows 10? Стоит ли обновляться с семёрки до 10 это уже вопрос другой, а если кот уже решил для себя перейти с windows 7 на windows 10 то в этой статье я расскажу как это сделать . Тем более, что компания Microsoft предоставляет обновление в течении года абсолютно бесплатно и всем желающим. Вот он то и предназначен для перехода с ОС Windows 7 до ОС Windows 10 бесплатно. Если у вас такого значка в трее нет, значит у вас, отключено автоматическое обновления системы. Если такой значок уже у вас светится, то сразу переходим к ПУНКТУ 2.

ПУНКТ 1.
Чтобы включить автоматическое обновление в своей системе, нажмите правой кнопкой мыши по значку «Компьютер» на рабочем столе либо в меню «Пуск» и выбираем пункт «Свойства».
Далее, выбираем раздел «Центр обновления Windows». В этом окне, выбираем пункт «Настройка параметров» и установите настройку «Устанавливать обновления автоматически (рекомендуется)».
После данных «манипуляций», компьютер сам начнет искать и устанавливать последние обновления для вашей ОС Windows. Если логотип Windows не появился в системном трее, значит, установились не все обновления, а именно, произошла установка только обязательных обновлений. Необходимо еще установить необязательные. Для этого, снова откройте центр обновлений (смотрите выше) и выбирайте установку необязательных. Далее, вам необходимо отметить галочкой те необязательные, которые необходимо установить. Для этого, выберите все обновления (поставьте «птички») до пункта Window 7 Language Packs — это языковые пакеты, они в данном случае, Вам точно не нужны. Нажимаем кнопку «OK». Далее, нажмите на кнопку «Установить обновления» и какое то время, ждите окончания процедуры загрузки и установки обновлений (зависит от скорости вашего Интернет-соединения и мощности компьютера), после чего в системном трее, должен появиться значок логотипа Microsoft Windows.',
        ]);

        $this->insert('pages', [
            'title' => 'Как почистить компьютер от ненужных файлов и мусора',
            'alias' => 'kak-pochistit-kompyuter-ot-nenuzhnyx-fajlov-i-musora',
            'intro' => 'Раньше я уже писал о том, как почистить компьютер или ноутбук от ненужного мусора с помощью программы AceUtilities. Но многие пользователи жаловались на ту статью поскольку программа в основном на английском языке. Время идет и на рынке программного обеспечения появляются новые продукты для диагностики ПК. В этой статье я протестирую программу для',
            'content' => 'Раньше я уже писал о том, как почистить компьютер или ноутбук от ненужного мусора с помощью программы AceUtilities. Но многие пользователи жаловались на ту статью поскольку программа в основном на английском языке. Время идет и на рынке программного обеспечения появляются новые продукты для диагностики ПК. В этой статье я протестирую программу для очистки ПК Kerish Doctor 2015. Программа на русском языке, но платная. Кстати стоимость лицензии не такая уж и дорогая. На 3 компа на 1 год всего 390 рублей. Можно скооперироваться с друзьями и выйдет всего по 150 р. (примерно с учетом всяких комиссий) в год. Не такая уж и большая цена для стабильной работы компьютера. У меня установлена Windows 7, но разработчик указал, что она работает на всех ОС начиная от XP и до последнего выпуска Windows 10. Запускается стандартный установщик. Что порадовало, так это то, что в нагрузку не устанавливается всякая лишняя R30;ерня, типа mail.ru. «Разработчики, если Вы читаете эту статью, знайте, что пользователя бесит, когда без его ведома с программой в нагрузку ставятся всякие дополнительные порнограммы типа менеджера браузеров от Яндекса». Теперь можно приступать к очистке компьютера от мусорных файлов. Я сразу выбираю раздел «Обслуживание«. Кстати приятно удивлен тем, что Kerish Doctor 2015 не только чистит компьютер от ненужных файлов но и делает еще много чего другого. Но в этой статье я не буду рассматривать все возможности по отдельности, так как это займет уйму времени и места на странице. Кто любит поковырять программы, тому безусловно будет интересно рассмотреть все разделы. У меня статья рассчитана на начинающего пользователя, поэтому я сразу вваливаю «Полную проверку»',
        ]);

        $this->insert('pages', [
            'title' => 'Как поставить пароль на флешку',
            'alias' => 'kak-postavit-parol-na-fleshku',
            'intro' => 'Сегодня я расскажу как поставить пароль на флешку. Наверняка каждый из нас имеет флешку в кармане или сумочке, на которой имеются какие то данные. Флешки как и все остальные вещи имеют свойство теряться, а поскольку они имеют маленький размер, то и потеряться у нее больше шансов. Ладно если на ней',
            'content' => 'Сегодня я расскажу как поставить пароль на флешку. Наверняка каждый из нас имеет флешку в кармане или сумочке, на которой имеются какие то данные. Флешки как и все остальные вещи имеют свойство теряться, а поскольку они имеют маленький размер, то и потеряться у нее больше шансов. Ладно если на ней какие нибудь неважные данные, а если у Вас там важные документы или семейные фотографии? То что тогда делать? — Ставить пароль, вот что!
Пароль будем ставить встроенными средствами Windows, без всяких программ. Называется она BitLocker. Но в этом деле есть ограничения, не все редакции Windows поддерживают эту функцию.
Данная функция доступна в следующих ОС: Открываем проводник файлов (Мой компьютер) и жмем по флешке правой кнопкой мышки тем самым вызывая контекстное меню и ищем функцию BitLocker. Если она у Вас присутствует, то все хорошо, если же её нету, то это значит что Ваша система не поддерживает такой метод шифрования и  запаролить флешку Вы не сможете. Далее в открывшемся окне устанавливаем галочку на пункте — «Использовать пароль для снятия блокировки флешки«. После чего придумываем надежный пароль, который должен быть не менее 8 символов и содержать как буквы так и цифры, в противном случае программа нас дальше не пустит. Жмем — Далее. Итак, задали пароль. Теперь программа предлагает нам создать и сохранить ключ для восстановления доступа к данным на флешке на случай если мы забыли пароль. Можно распечатать его на принтере, а можно сохранить на компьютере. Я буду сохранять его на компьютере. Выбираем пункт — Сохранить в файл. Далее предлагается какую часть диска требуется зашифровать. Поскольку шифрование занимает относительно долгое время, то я выбрал первый пункт, то есть шифрование только занятого пространства. Если у Вас супер секретная информация на флешке, то выбирайте второй пункт и тогда будет зашифрована полностью вся флешка. Жмем — Далее.',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pages');
    }
}
