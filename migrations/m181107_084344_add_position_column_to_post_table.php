<?php

use yii\db\Migration;

/**
 * Handles adding position to table `post`.
 */
class m181107_084344_add_position_column_to_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'userId', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'userId');
    }
}
