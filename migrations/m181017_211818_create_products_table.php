<?php

use yii\db\Migration;

/**
 * Handles the creation of table `products`.
 */
class m181017_211818_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'alias' => $this->string(),
            'price' => $this->integer(),
            'description' => $this->text(),
        ]);

        $this->insert('products', [
            'title' => 'Everest Home',
            'alias' => 'C1256',
            'price' => 17999,
            'description' => 'Так как компьютер покупался под большинство современных игр, поделюсь тестами некоторых из них: 
                                В PUBG можно спокойно играть на низких/средних настройках и в full hd разрешении при стабильных 60-80 фпс.
                                Battlefield 1 на тех же низких/средних настройках не нагружал фпс ниже 60 кадров (при игре на сервере с 64 игроками)
                                Те же Dota 2, CS:GO и WoT спокойно идут на ультрах не ниже 60 фпс.',
        ]);

        $this->insert('products', [
            'title' => 'Asus Zen',
            'alias' => 'C7758',
            'price' => 53999,
            'description' => 'Экран 23.8" IPS (1920x1080) Full HD / Intel Core i7-7700HQ (2.8 - 3.8 ГГц) / RAM 32 ГБ / HDD 1 ТБ + SSD 512 ГБ / nVidia GeForce GTX 1050, 4 ГБ / без ОД / LAN / Wi-Fi /                                       Bluetooth / кардридер / веб-камера / Windows 10 Home 64bit / 6 кг / серебристый / клавиатура + мышь',
        ]);

        $this->insert('products', [
            'title' => 'Lenovo IdeaCentre',
            'alias' => 'C6356',
            'price' => 27359,
            'description' => 'Экран 23.8" (1920x1080) Full HD / Intel Core i5-7400 (3.0 - 3.5 ГГц) / RAM 8 ГБ / HDD 1 ТБ / nVidia GeForce GTX 960, 2 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / веб-камера / без ОС / 9.15 кг / черный / клавиатура + мышь',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
    }
}
