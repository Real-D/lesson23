<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 15:03
 */

namespace app\models;

use yii\db\ActiveRecord;
use yii\base\Model;

class Product extends ActiveRecord
{
    public static function tableName()
    {
        return 'products';
    }

    public function rules()
    {
        return [
            [['alias', 'description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['price'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'alias' => 'Артикл',
            'price' => 'Цена',
            'description' => 'Описание'
        ];
    }
}