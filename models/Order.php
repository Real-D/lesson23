<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 20:51
 */

namespace app\models;

use yii\db\ActiveRecord;
use yii\base\Model;

class Order extends ActiveRecord
{
    public static function tableName()
    {
        return 'Orders';
    }

    public function rules()
    {
        return [
            [['phone', 'feedback'], 'string'],
            [['email'], 'email'],
            [['userId'], 'integer'],
            [['customer_name'], 'string', 'max' => 255],
            [
                'userId',
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['userId' => 'id']
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_name' => 'Имя пользователя',
            'email' => 'Почтовый ящик',
            'phone' => 'Телефон',
            'feedback' => 'коментарий или заказ',
            'userId' => 'User ID'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['userId' => 'id']);
    }
}