<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
   public static function tableName()
   {
       return 'users';
   }

   public function rules()
   {
       return [
           ['id', 'integer'],
           [['username', 'password', 'authKey', 'accessToken'], 'string'],
           ['username', 'unique'],
       ];
   }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return self::find()->where(['accessToken' => $token])->one();
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return self::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       /* var_dump($password, $this->password, \Yii::$app->getSecurity()->generatePasswordHash(admin));
        die;*/
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return mixed
     */
    public function getOrders()
    {
        return $this->hasOne(Order::className(), ['id' => 'userId']);
    }

    /**
     * @param $customer_name
     * @param $email
     * @param $phone
     * @param $feedback
     * @return bool
     */
    public function addOrder($customer_name, $email, $phone, $feedback)
    {
        $order = new Order();
        $order->customer_name = $customer_name;
        $order->email = $email;
        $order->phone = $phone;
        $order->feedback = $feedback;
        $order->userId = $this->id;

        return $order->save();
    }

}
