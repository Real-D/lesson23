<?php
/**
 * Created by PhpStorm.
 * User: Админ
 * Date: 19.10.2018
 * Time: 16:21
 */

namespace app\models;

use yii\db\ActiveRecord;
use yii\base\Model;

class Page extends ActiveRecord
{
    public static function tableName()
    {
        return 'Pages';
    }

    public function rules()
    {
        return [
            [['alias', 'content', 'intro'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'alias' => 'Артикл',
            'intro' => 'краткое описание',
            'content' => 'текст'
        ];
    }
}