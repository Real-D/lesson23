<div>
    <p><a href="create">Создать</a></p>
</div>
<div>
    <?php foreach ($pages as $page) : ?>
        <div>
            <h3><?= $page->title ?></h3>
            <p><?= $page->intro ?></p>
            <p><a href="view?id=<?= $page->id ?>">Подробнее...</a>  (<a href="update?id=<?=$page->id ?>">Редактировать</a>/<a href="delete?id=<?=$page->id ?>">Удалить</a>)</p>
        </div>
    <?php endforeach; ?>
</div>