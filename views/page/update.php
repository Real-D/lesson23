<div>
    <h2>Update page</h2>
    <form action="/page/update" method="post" class="form-horizontal">
        <input type="hidden" name="<?= \Yii::$app->request->csrfParam; ?>" value="<?= \Yii::$app->request->csrfToken; ?>" />
        <input type="hidden" name="id" id="id" class="form-control" value="<?=$page->id?>">
        <div class="form-group">
            <label class="control-label">Title</label>
            <input type="text" name="title" id="title" class="form-control" value="<?=$page->title?>">
        </div>
        <div class="form-group">
            <label class="control-label">Alias</label>
            <input type="text" name="alias" id="alias" class="form-control" value="<?=$page->alias?>">
        </div>
        <div class="form-group">
            <label class="control-label">Intro</label>
            <textarea name="intro" id="intro" class="form-control"><?=$page->intro?></textarea>
        </div>
        <div class="form-group">
            <label class="control-label">Content</label>
            <textarea name="content" id="content" class="form-control"><?=$page->content?></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
</div>