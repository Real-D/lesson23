<div>
    <h2>Update product</h2>
    <form action="/content/update" method="post" class="form-horizontal">
        <input type="hidden" name="<?= \Yii::$app->request->csrfParam; ?>" value="<?= \Yii::$app->request->csrfToken; ?>" />
        <input type="hidden" name="id" id="id" class="form-control" value="<?=$product->id?>">
</div>
        <div class="form-group">
            <label class="control-label">Title</label>
            <input type="text" name="title" id="title" class="form-control" value="<?=$product->title?>">
        </div>
        <div class="form-group">
            <label class="control-label">Alias</label>
            <input type="text" name="alias" id="alias" class="form-control" value="<?=$product->alias?>">
        </div>
        <div class="form-group">
            <label class="control-label">Price</label>
            <input type="text" name="price" id="price" class="form-control" value="<?=$product->price?>">
        </div>
        <div class="form-group">
            <label class="control-label">Description</label>
            <textarea name="description" id="description" class="form-control"><?=$product->description?>"</textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">Update</button>
        </div>
    </form>
</div>