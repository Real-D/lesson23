<div>
    <p><a href="create">Создать</a></p>
</div>
<ul>
    <?php foreach ($products as $product) :?>
        <li>
            <strong><a href="view?id=<?= $product->id ?>"><?= $product->title?></a></strong> (<a href="update?id=<?=$product->id ?>">Изменить</a>/<a
                    href="delete?id=<?=$product->id ?>">Удалить</a>)
        </li>
    <?php endforeach ?>
</ul>

<?php
