<div>
    <h2>Create product</h2>
    <form action="/content/create" method="post" class="form-horizontal">
        <input type="hidden" name="<?= \Yii::$app->request->csrfParam; ?>" value="<?= \Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
        <label class="control-label">Title</label>
        <input type="text" name="title" id="title" class="form-control">
        </div>
        <div class="form-group">
        <label class="control-label">Alias</label>
        <input type="text" name="alias" id="alias" class="form-control">
        </div>
        <div class="form-group">
        <label class="control-label">Price</label>
        <input type="text" name="price" id="price" class="form-control">
        </div>
        <div class="form-group">
        <label class="control-label">Description</label>
        <textarea name="description" id="description" class="form-control"></textarea>
        </div>
        <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
</div>