<div>
    <h2>Create Order</h2>
    <form action="/order/create" method="post" class="form-horizontal">
        <input type="hidden" name="<?= \Yii::$app->request->csrfParam; ?>" value="<?= \Yii::$app->request->csrfToken; ?>" />
        <div class="form-group">
        <input type="hidden" name="customer_name" id="customer_name" class="form-control" value="customer_name">
        </div>
        <div class="form-group">
        <label class="control-label">Email</label>
        <input type="text" name="email" id="email" class="form-control">
        </div>
        <div class="form-group">
        <label class="control-label">Телефон</label>
        <input type="text" name="phone" id="phone" class="form-control">
        </div>
        <div class="form-group">
        <label class="control-label">Коментарий/Заказ</label>
        <textarea name="feedback" id="feedback" class="form-control"></textarea>
        </div>
        <input type="hidden" name="userId" id="userId" class="form-control" value="<?=$_SESSION['__id']?>">
</div>
        <div class="form-group">
                <button type="submit" class="btn btn-success">Create</button>
        </div>
    </form>
</div>